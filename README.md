# Automate Your Bot!

💰 自動化簽到，每日拿錢錢 💰

使用 GitHub Actions 跑機器人，不需要準備自己的機器，完全免費！

## 2022/08 更

這個專案在 [GitHub](https://github.com/wdzeng/bot-automation) 上已經被 ban 了哭哭。所有開發已經停止，但[蝦皮簽到機器人原始碼](https://github.com/wdzeng/shopee-coins-bot)依然持續在維護。

如果你要將此專案 clone 到 GitHub 上，請注意自身安危；永言配命，自求多福。調整過後的 workflow 可能有 bug，因為作者不想拿到 GitHub 上測試。如果你覺得有問題，請聯絡開發者，或在 GitLab 上開 issue。

## 簽到機器人列表

[![shopee](img/shopee.png)](https://shopee.tw/)

每日簽到領蝦幣！

- [📔 點我開始](docs/shopee-gha-inst.md)
- [👷‍ 疑難排解](docs/shopee-issues.md)
- [🤷‍♂️ FAQ](docs/shopee-faq.md)
- [🤖 蝦皮簽到機器人原始碼](https://github.com/wdzeng/shopee-coins-bot)

[![pinkoi](img/pinkoi.png)](https://www.pinkoi.com/)

每日簽到領 P 幣！使用說明製作中。

- [🤖 Pinkoi 簽到機器人原始碼](https://github.com/wdzeng/pinkoi-coins-bot)

開發者近期非常忙碌，Pinkoi 最快要等到七月才會完工 😢😢 以後有時間可能會做其他家的機器人吧（歡迎許願

要接收進度通知的話，請點擊畫面右上方 watch 按鈕關注本專案。

## 更新

作者會三不五時更新機器人，且**不會有主動通知**，你需要自己留意。你可以按照以下方式套用最新更新。注意這是在你自己的專案頁面的畫面，不是我的專案頁面。

> ![update](img/update.png)

如果你發現按鈕沒有出現在畫面上，表示已經是最新版。

## 聯繫開發者

**除非你有涉及隱私的訊息要傳達**，否則請一律到 [issue](https://github.com/wdzeng/bot-automation/issues?q=) 區發問，不要寄電子郵件。所謂的涉及隱私，是指訊息內容涉及個資或帳號密碼。

- me@hyperbola.me

聯繫開發者時，如果情況必要請主動提及你的 GitHub ID，讓開發者協助你！
