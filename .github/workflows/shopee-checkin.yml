name: 蝦皮每日簽到

on:
  workflow_dispatch: ~
  schedule:
    # * is a special character in YAML so you have to quote this string
    # Run cron at 00:10 UTC+8
    - cron: '10 16 * * *'

env:
  BOT_IMAGE: ghcr.io/wdzeng/shopee-coins-bot:1

jobs:
  checkin:
    runs-on: ubuntu-latest
    steps:
      - name: Check inputs
        if: ${{ github.event_name == 'workflow_dispatch' }}
        env:
          SHOPEE_USERNAME: ${{ secrets.SHOPEE_USERNAME }}
          SHOPEE_PASSWORD: ${{ secrets.SHOPEE_PASSWORD }}
          AES_KEY: ${{ secrets.AES_KEY }}
        run: |
          exit_code=0
          if [[ -z "$SHOPEE_USERNAME" ]]; then
            echo '::error::你沒有設定帳號，請檢查設定或參考使用說明。帳號的 Name 欄位必須是 "SHOPEE_USERNAME"（不含引號），請再確認一次。'
            exit_code=87
          fi
          if [[ -z "$SHOPEE_PASSWORD" ]]; then
            echo '::error::你沒有設定密碼，請檢查設定或參考使用說明。密碼的 Name 欄位必須是 "SHOPEE_PASSWORD"（不含引號），請再確認一次。'
            exit_code=87
          fi
          if [[ -z "$AES_KEY" ]]; then
            echo '::error::你沒有設定金鑰，請檢查設定或參考使用說明。金鑰的 Name 欄位必須是 "AES_KEY"（不含引號），請再確認一次。'
            exit_code=87
          elif [[ ! "$AES_KEY" =~ ^[0-9a-fA-F]{64}$ ]]; then
            echo '::error::金鑰不符合格式，必須是 64 個 0-9 或 a-f 的英數字組合。'
            exit_code=87
          fi

          if [[ $exit_code -eq 87 ]]; then
            echo '::error::請參考說明文件：https://github.com/wdzeng/bot-automation/blob/main/docs/shopee-gha-inst.md。'
          fi
          exit $exit_code

      - name: Run checkin bot
        uses: wdzeng/shopee-coins-bot@main
        with:
          key: ${{ secrets.AES_KEY }}
          username: ${{ secrets.SHOPEE_USERNAME }}
          password: ${{ secrets.SHOPEE_PASSWORD }}
          login: ${{ (github.event_name == 'schedule') && 'credential' || 'password' }}

      - name: Checkout to credential branch
        id: checkout
        uses: actions/checkout@v3
        with:
          ref: credential
        # Ignore error if the credential branch does not exist while the user is using password login
        continue-on-error: ${{ github.event_name == 'workflow_dispatch' }}

      - name: Decrypt credential
        # Runs only if the checkout succeeded, and the user is using cookie login
        if: ${{ steps.checkout.outcome == 'success' && github.event_name != 'workflow_dispatch' }}
        shell: bash -e {0}
        run: |
          base64 -d credential | openssl aes-256-cbc -d -pass pass:${{ secrets.AES_KEY }} -pbkdf2 > plain_credential

        # Ignore error if the decryption did not succeed while the login method is password
        continue-on-error: ${{ github.event_name == 'workflow_dispatch' }}

      - name: Run shopee checkin bot
        id: bot
        env:
          SHOPEE_USERNAME: ${{ secrets.SHOPEE_USERNAME }}
          SHOPEE_PASSWORD: ${{ secrets.SHOPEE_PASSWORD }}
        shell: bash -e {0}
        run: |
          FLAGS='-f'
          if [[ '${{ github.event_name }}' != 'workflow_dispatch' ]]; then
            FLAGS+=' --no-sms'
          fi

          # if the checkout succeeded then mount current directory or else
          # the subdirectory `.credential`
          if [[ '${{ steps.checkout.outcome }}' == 'success' ]]; then
            MOUNT="-v $(pwd):/repo"
          else
            mkdir .credential
            MOUNT="-v $(pwd)/.credential:/repo"
          fi

          # Create a folder to save screenshot
          mkdir -p /tmp/shopee_bot

          docker run \
            --user $UID \
            -e USERNAME="$SHOPEE_USERNAME" \
            -e PASSWORD="$SHOPEE_PASSWORD" \
            $MOUNT \
            -t \
            -v /tmp/shopee_bot:/screenshot \
            $BOT_IMAGE \
            -c /repo/plain_credential \
            $FLAGS \
            -s /screenshot || {
              exit_code=$?
            }

          if [[ $exit_code -eq 3 || $exit_code -eq 69 ]]; then
            echo '::error::因為嘗試次數過督被蝦皮 ban 了，請明天再試。'
          elif [[ $exit_code -eq 4 ]]; then
            echo '::error::操作逾時。如果你沒有收到簡訊，可能是蝦皮的問題。如果你有收到簡訊，請回報開發者。'
          elif [[ $exit_code -eq 5 ]]; then
            echo '::error::觸發電子郵件驗證。非常遺憾，機器人尚不支援電子郵件驗證。'
          elif [[ $exit_code -eq 87 ]]; then
            echo '::error::帳號或密碼錯誤。'
          elif [[ $exit_code -ne 0 ]]; then
            # error code should be 88
            echo "::error::發生不明錯誤，錯誤代碼為 $exit_code。請回報開發者。"
          fi

          echo "::set-output name=exit_code::$exit_code"
          exit $exit_code

      - name: Upload error screenshot
        if: ${{ failure() && (steps.bot.outputs.exit_code == 4 || steps.bot.outputs.exit_code == 88) }}
        uses: actions/upload-artifact@v3
        with:
          name: screenshot
          path: /tmp/shopee_bot/screenshot.png

      - name: Encrypt credential
        shell: bash -e {0}
        run: |
          # If the checkout did not succeeded then the credentials is placed
          # at .credential directory
          if [[ '${{ steps.checkout.outcome }}' != 'success' ]]; then
            cd .credential
          fi
          cat plain_credential | openssl aes-256-cbc -pass pass:${{ secrets.AES_KEY }} -pbkdf2 | base64 > shopee && \
          rm plain_credential

          # Remove old credential file
          rm -f credential

      - name: Update credential
        uses: peaceiris/actions-gh-pages@v3
        with:
          github_token: ${{ github.token }}
          publish_branch: credential
          publish_dir: ${{ steps.checkout.outcome == 'success' && '.' || '.credential' }}
          allow_empty_commit: true
          force_orphan: true
          user_name: bot
          user_email: bot@hyperbola.me
          commit_message: update credential
          enable_jekyll: true # dont generate .nojekyll file

      - name: Echo hint message
        shell: bash -e {0}
        run: |
          if [[ '${{ github.event_name }}' == 'workflow_dispatch' ]]; then
            echo "::notice::簽到成功囉，從接下來開始每天凌晨 00:10 會進行排程自動簽到；不過 GitHub Action 的排程常常有數小時的延誤，所以如果發現機器人沒有準時運作的話，就再等一下吧。另外，如果你看到上面有個 \"The process '/usr/bin/git' failed with exit code 1\" 的錯誤訊息，那是正常的，不用理會。"
          elif [[ '${{ github.event_name }}' == 'schedule' ]]; then
            echo "::notice::$(TZ=Asia/Taipei date +%F) 自動簽到成功！"
          fi
